# The order of packages is significant, because pip processes them in the order
# of appearance. Changing the order has an impact on the overall integration
# process, which may cause wedges in the gate later.
pbr>=4.0.0 # Apache-2.0

alembic>=1.6.5 # MIT
neutron-lib>=3.4.0 # Apache-2.0
oslo.i18n>=3.20.0 # Apache-2.0
oslo.config>=9.0.0 # Apache-2.0
oslo.log>=4.5.0 # Apache-2.0
oslo.service>=2.8.0 # Apache-2.0
oslo.utils>=4.8.0 # Apache-2.0
requests>=2.18.0 # Apache-2.0
six>=1.10.0 # MIT
SQLAlchemy>=1.4.23 # MIT

# These repos are installed from git in OpenStack CI if the job
# configures them as required-projects
neutron # Apache-2.0

# The comment below indicates this project repo is current with neutron-lib
# and should receive neutron-lib consumption patches as they are released
# in neutron-lib. It also implies the project will stay current with TC
# and infra initiatives ensuring consumption patches can land.
# neutron-lib-current
