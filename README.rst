===============================
networking-arista
===============================

Arista Networking drivers

* Free software: Apache license
* Source: https://opendev.org/x/networking-arista
* Bug: https://bugs.launchpad.net/networking-arista
